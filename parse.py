#!/usr/bin/python

from __future__ import with_statement
from stat import *
from xml.dom import minidom
from xml.dom import Node
from xml.parsers import expat
from optparse import OptionParser

import sys
import shutil
import getopt
import platform
import os
import string

class MachineParameters(object):

    def __init__(self, machineUuid = "", machineName = "", hdId = ""):

        self.__uuid = machineUuid
        self.__name = machineName
        self.__hdid = hdId

    def __setUuid(self,machineUuid):
        self.__uuid = machineUuid

    def __getUuid(self):
        return self.__uuid

    def __setName(self,machineName):
       self.__name = machineName

    def __getName(self):
        return self.__name

    def __setHdid(self,hdid):
       self.__hdid = hdid

    def __getHdid(self):
        return self.__hdid

    uuid = property(fget = __getUuid,
                    fset = __setUuid)

    name = property(fget = __getName,
                    fset = __setName)

    hdid = property(fget = __getHdid,
                    fset = __setHdid)

def getMachineVersion(docname):
    try:
        xmldoc = minidom.parse(docname)
        root = xmldoc.documentElement

        if root.nodeName == "VirtualBox":
            rval = root.getAttribute("version")
            root.unlink()
            return rval
        else:
            root.unlink()
            print "Document of the wrong type"
            return None

    except IOError, e:
        raise
    except expat.ExpatError, e:
        raise
    except KeyboardInterrupt:
        root.unlink()
        raise

#===============================================================================
#    reflist = xmldoc.getElementsByTagName('VirtualBox')
#
#    print xmldoc.nodeName
#
#    if type(reflist[0]) == type(None):
#        print "Document of the wrong type"
#        return None
#    else:
#        return reflist[0].attributes["version"].value
#===============================================================================

def changeMachineVersion(docname, version):
    try:

        xmldoc = minidom.parse(docname)
        root = xmldoc.documentElement

        if root.nodeName == "VirtualBox":
            oldVersion = root.getAttribute("version")

            root.setAttribute("version", version)

            machineNode = root.childNodes[1]

            machineNodeChildrens = machineNode.childNodes

            for i in machineNodeChildrens:
                if i.nodeName == "Hardware":

                    for j in i.childNodes:
                        if j.nodeName == "DVDDrive":
                            for k in j.childNodes:
                                if k.nodeType != Node.TEXT_NODE:
                                    j.removeChild(k)

            with open(docname, "w") as fp:
                fp.write(xmldoc.toxml())

            root.unlink()

            if version.startswith("1.3") and oldVersion.startswith("1.2"):
                from1_2To1_3(docname)

        else:
            print "Document of the wrong type"
            return None

    except IOError, e:
        raise
    except NameError, e:
        raise
    except expat.ExpatError, e:
        raise
    except KeyboardInterrupt:
        raise

#===============================================================================
# def getMachineVersion(docname):
#
#    doc = libxml2.parseFile(docname)
#
#    if doc is None:
#        print "Document not parsed successfully."
#        return None
#
#    cur = doc.getRootElement()
#
#
#    if cur is None:
#        print "Document not parsed successfully"
#        doc.freeDoc()
#        return None
#
#
#    if cur.name != "VirtualBox":
#        print "Document of the wrong type"
#        doc.freeDoc()
#        return None
#    else:
#        machine_version = cur.prop("version")
#        doc.freeDoc()
#        return machine_version
#===============================================================================

def from1_2To1_3(docname):
    try:
        xmldoc = minidom.parse(docname)

        root = xmldoc.documentElement

        if root.nodeName == "VirtualBox":
            machineNode = root.childNodes[1]

            machineNodeChildrens = machineNode.childNodes

            for i in machineNodeChildrens:
                if i.nodeName == "Hardware":

                    sataController = xmldoc.createElement("SATAController")

                    sataController.setAttribute("enabled","false")
                    sataController.setAttribute("PortCount","1")
                    sataController.setAttribute("IDE0MasterEmulationPort","0")
                    sataController.setAttribute("IDE0SlaveEmulationPort" ,"1")
                    sataController.setAttribute("IDE1MasterEmulationPort","2")
                    sataController.setAttribute("IDE1SlaveEmulationPort","3")

                    i.appendChild(sataController)

                    for j in i.childNodes:
                        if j.nodeName == "CPU":
                            pae = xmldoc.createElement("PAE")
                            pae.setAttribute("enabled","false")

                            j.appendChild(pae)

                        elif j.nodeName == "Guest":
                            guest = xmldoc.createElement("Guest")
                            guest.setAttribute("memoryBalloonSize","0")
                            guest.setAttribute("statisticsUpdateInterval","0")
                            i.replaceChild(guest,j)

                        elif j.nodeName == "RemoteDisplay":
                            j.setAttribute("authType","Null")

                        elif j.nodeName == "AudioAdapter":
                            driver = j.getAttribute("driver")

                            if driver == "null":
                                j.setAttribute("driver","Null")

                        elif j.nodeName == "Uart":
                            uart = xmldoc.createElement("UART")

                            port1 = j.childNodes[1].cloneNode(0)
                            port2 = j.childNodes[3].cloneNode(0)

                            uart.appendChild(port1)
                            uart.appendChild(port2)

                            i.replaceChild(uart,j)

                        elif j.nodeName == "Lpt":
                            lpt = xmldoc.createElement("LPT")

                            port1 = j.childNodes[1].cloneNode(0)
                            port2 = j.childNodes[3].cloneNode(0)

                            lpt.appendChild(port1)
                            lpt.appendChild(port2)

                            i.replaceChild(lpt,j)

                        elif j.nodeName == "BIOS":
                            for k in j.childNodes:
                                if k.nodeName == "BootMenu":
                                    k.setAttribute("mode","MessageAndMenu")

                if i.nodeName == "HardDiskAttachments":
                    for j in i.childNodes:
                        if j.nodeName == "HardDiskAttachment":
                            j.setAttribute("bus","IDE")
                            j.setAttribute("device","0")
                            j.setAttribute("channel","0")

            with open(docname, "w") as fp:
                fp.write(xmldoc.toxml())

            root.unlink()

        else:
            print "Document of the wrong type"
            return None

    except IOError:
        raise
    except NameError, e:
        raise
    except expat.ExpatError, e:
        raise
    except KeyboardInterrupt:
        raise

def modifyConfigFile(docname, vm):
    try:
        if not isinstance(vm, MachineParameters):
            return

        src = "Machines/" + vm.name + "/" + vm.name + ".xml"
        filePath = "VDI/" + vm.name + ".vdi"

        xmldoc = minidom.parse(docname)

        root = xmldoc.documentElement

        if root.nodeName == "VirtualBox":

             globalNode = root.childNodes[1]

             globalNodeChildrens = globalNode.childNodes

             machineEntry = checkMachineEntry(globalNode,vm.uuid)
             hdEntry = checkHdEntry(globalNode,vm.hdid)

             if not machineEntry and not hdEntry:

                 for i in globalNodeChildrens:
                     if i.nodeName == "MachineRegistry":
                         machineEntry = xmldoc.createElement("MachineEntry")

                         machineEntry.setAttribute("uuid",vm.uuid)
                         machineEntry.setAttribute("src",src)

                         i.appendChild(machineEntry)

                     elif i.nodeName == "DiskRegistry":
                        for j in i.childNodes:
                            if j.nodeName == "HardDisks":
                                hardDisk = xmldoc.createElement("HardDisk")

                                hardDisk.setAttribute("uuid",vm.hdid)
                                hardDisk.setAttribute("type","normal")

                                virtualDiskImage = xmldoc.createElement("VirtualDiskImage")
                                virtualDiskImage.setAttribute("filePath",filePath)

                                hardDisk.appendChild(virtualDiskImage)

                                j.appendChild(hardDisk)

             elif not machineEntry and hdEntry:
                 for i in globalNodeChildrens:
                     if i.nodeName == "MachineRegistry":
                         machineEntry = xmldoc.createElement("MachineEntry")

                         machineEntry.setAttribute("uuid",vm.uuid)
                         machineEntry.setAttribute("src",src)

                         i.appendChild(machineEntry)

             elif machineEntry and not hdEntry:
                 for i in globalNodeChildrens:

                     if i.nodeName == "DiskRegistry":
                        for j in i.childNodes:
                            if j.nodeName == "HardDisks":
                                hardDisk = xmldoc.createElement("HardDisk")

                                hardDisk.setAttribute("uuid",vm.hdid)
                                hardDisk.setAttribute("type","normal")

                                virtualDiskImage = xmldoc.createElement("VirtualDiskImage")
                                virtualDiskImage.setAttribute("filePath",filePath)

                                hardDisk.appendChild(virtualDiskImage)

                                j.appendChild(hardDisk)

             elif machineEntry and hdEntry:
                 pass

             with open(docname,"w") as fp:
                 fp.write(xmldoc.toxml())

             root.unlink()

        else:
            print "Document of the wrong type"
            return None

    except IOError, error:
        raise
    except NameError, e:
        raise
    except expat.ExpatError, e:
        raise
    except KeyboardInterrupt:
        raise

def checkMachineEntry(globalNode, uuid):

    globalNodeChildrens = globalNode.childNodes

    for i in globalNodeChildrens:
        if i.nodeName == "MachineRegistry":

            for j in i.childNodes:
                if j.nodeName == "MachineEntry":
                    currentUuid = j.getAttribute("uuid")

                    if currentUuid == uuid:
                        return 1

    return 0

def checkHdEntry(globalNode, hdid):

    globalNodeChildrens = globalNode.childNodes

    for i in globalNodeChildrens:
        if i.nodeName == "DiskRegistry":

            for j in i.childNodes:
                if j.nodeName == "HardDisks":

                    for k in j.childNodes:
                        if k.nodeName == "HardDisk":
                            currentHdid = k.getAttribute("uuid")

                            if currentHdid == hdid:
                                return 1

    return 0

#===============================================================================
# def changeMachineVersion(docname, version):
#    doc = libxml2.parseFile(docname)
#
#    if doc is None:
#        print "Document not parsed successfully"
#        return None
#
#    cur = doc.getRootElement()
#
#    if cur is None:
#        print "Document not parsed successfully"
#        doc.freeDoc()
#        return None
#    elif cur.name != "VirtualBox":
#        print "Document of the wrong type"
#        doc.freeDoc()
#        return None
#    else:
#        oldVersion = cur.prop("version")
#
#        cur.setProp("version", version)
#
#        libxml2.keepBlanksDefault(0)
#        doc.saveFormatFile(docname, 1)
#
#        if version[:3] == "1.3" and oldVersion[:3] == "1.2":
#            from1_2To1_3(docname)
#===============================================================================

def getMachineParameters(docname):
    try:
        param = MachineParameters()
        xmldoc = minidom.parse(docname)

        root = xmldoc.documentElement

        if root.nodeName == "VirtualBox":
            machineNode = root.childNodes[1]

            param.uuid = machineNode.getAttribute("uuid")
            param.name = machineNode.getAttribute("name")

            machineNodeChildrens = machineNode.childNodes

            for i in machineNodeChildrens:
                if i.nodeName == "HardDiskAttachments":
                    for j in i.childNodes:
                        if j.nodeName == "HardDiskAttachment":
                            param.hdid = j.getAttribute("hardDisk")

                            return param

            root.unlink()

        else:
            print "Document of the wrong type"
            return None

    except IOError, error:
        raise
    except NameError, e:
        raise
    except expat.ExpatError, e:
        raise
    except KeyboardInterrupt:
        raise

#===============================================================================
# def getMachineParameters(docname):
#
#    try:
#        param = MachineParameters()
#
#        doc = libxml2.parseFile(docname)
#
#        cur =  doc.getRootElement()
#
#
#        if cur.name != "VirtualBox":
#            print "Document of the wrong type"
#            doc.freeDoc()
#            return None
#
#        cur =  cur.children
#
#        while cur is not None:
#            if cur.name == "Machine":
#                param["uuid"] = cur.prop("uuid")
#                param["name"] = cur.prop("name")
#
#                cur2 = cur.children
#
#                while cur2 is not None:
#                    if cur2.name == "HardDiskAttachments":
#                        cur3 = cur2.children
#
#                        while cur3 is not None:
#                            if cur3.name == "HardDiskAttachment":
#                                param["hdid"] = cur3.prop("hardDisk")
#
#                            cur3 = cur3.next
#
#
#                    cur2 = cur2.next
#
#            cur = cur.next
#
#        doc.freeDoc()
#
#        return param
#
#    except libxml2.parserError:
#        print "Error parsing ", docname
#        return None
#    except libxml2.treeError:
#        print "Cannot determine document root"
#        return None
#===============================================================================

def getVirtualBoxConfigFile():
    plat = platform.platform(True, True)

    if "Darwin" in plat:
        return os.environ['HOME'] + "/Library/VirtualBox/VirtualBox.xml"
    elif "Linux" in plat:
        return os.environ['HOME'] + "/.VirtualBox/VirtualBox.xml"
    elif "Windows" in plat:
        return os.environ['HOMEPATH'] + "\\.VirtualBox\\VirtualBox.xml"

def moveMachineAndVDI(machinePath, VDIPath, vm):

    plat = platform.platform(True,True)
    try:
        if "Darwin" in plat:
            macMove(machinePath,VDIPath,vm)

        elif "Windows" in plat:
            winMove(machinePath,VDIPath,vm)
        elif "Linux" in plat:
            linMove(machinePath,VDIPath,vm)

    except IOError, e:
        raise
    except OSError, e:
        raise
    except NameError, e:
        raise
    except KeyboardInterrupt:
        raise

def macMove(machinePath,VDIPath,vm):
    try:
        home = os.environ['HOME']

        destDir = string.join([home,"/Library/VirtualBox"],"")

        machines = string.join([destDir,"/Machines/",vm.name], "")

        vdi = string.join([destDir,"/VDI/"],"")

        if not os.path.exists(machines):
            os.mkdir(machines)

        print "\nMoving " + machinePath +" to " + machines + "....\n\n"
        shutil.move(machinePath, machines)

        print "Moving " + VDIPath +" to " + vdi + "....\n\n"
        shutil.move(VDIPath, vdi)

    except IOError, e:
        raise
    except OSError, e:
        raise
    except NameError, e:
        raise
    except KeyboardInterrupt:
        raise

def linMove(machinePath,VDIPath,vm):
    try:
        home = os.environ['HOME']

        destDir = string.join([home,"/.VirtualBox"],"")

        machines = string.join([destDir,"/Machines/",vm.name], "")

        vdi = string.join([destDir,"/VDI"],"")

        if not os.path.exists(machines):
            os.mkdir(machines)

        if os.path.dirname(os.path.abspath(machinePath)) != machines:
            print "Moving " + machinePath +" to " + machines + " ..."
            shutil.move(machinePath, machines)

        if not os.path.exists(vdi):
            os.mkdir(vdi)

        if os.path.dirname(os.path.abspath(VDIPath)) != vdi:
            print "Moving " + VDIPath +" to " + vdi + " ..."
            shutil.move(VDIPath, vdi)

    except IOError, e:
        raise
    except OSError, e:
        raise
    except NameError, e:
        raise
    except KeyboardInterrupt:
        raise

def winMove(machinePath,VDIPath,vm):
    try:
        home = os.environ['HOMEPATH']

        destDir = string.join([home,"\\.VirtualBox"],"")

        machines = string.join([destDir,"\\Machines\\",vm.name], "")

        vdi = string.join([destDir,"\\VDI"],"")

        if not os.path.exists(machines):
            os.mkdir(machines)

        if os.path.dirname(os.path.abspath(machinePath)) != os.path.dirname(machines):
            print "Moving " + machinePath +" to " + machines + " ..."
            shutil.move(machinePath, machines)

        if not os.path.exists(vdi):
            os.mkdir(vdi)

        if os.path.dirname(os.path.abspath(VDIPath)) != os.path.dirname(vdi):
            print "Moving " + VDIPath +" to " + vdi + " ..."
            shutil.move(VDIPath, vdi)

    except IOError, e:
        raise
    except OSError, e:
        raise
    except NameError, e:
        raise
    except KeyboardInterrupt:
        raise

def createParser():
    usage = "usage: %prog vm_config_file.xml virtual_disk_image.vdi"

    parser = OptionParser(usage, version = "%prog 1.0.5")

    return parser



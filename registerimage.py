#!/usr/bin/python

from parse import *
from xml.parsers import expat

import sys

def main():
    try:
        parser = createParser()

        options, args = parser.parse_args()

        if len(args) != 2:
            parser.error("incorrect number of arguments")

        machineConfig = args[0]
        virtual = getVirtualBoxConfigFile()
        vdi = args[1]

        version = getMachineVersion(virtual)

        if not version:
            sys.exit(1)

        changeMachineVersion(machineConfig,version)

        param = getMachineParameters(machineConfig)

        modifyConfigFile(virtual,param)

        moveMachineAndVDI(machineConfig,vdi,param)

    except IOError, e:
        raise
    except OSError, e:
        raise
    except NameError, e:
        raise
    except KeyboardInterrupt:
        raise
    except expat.ExpatError, e:
        raise

if __name__ == "__main__":
    try:

        main()

    except IOError, e:
        print str(e)
        sys.exit(1)
    except OSError, e:
        print str(e)
        sys.exit(1)
    except NameError, e:
        print str(e)
        sys.exit(1)
    except expat.ExpatError, e:
        print str(e)
        sys.exit(1)
    except KeyboardInterrupt:
        sys.exit(1)

